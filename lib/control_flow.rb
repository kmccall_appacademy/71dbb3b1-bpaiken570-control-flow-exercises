# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |chr| str.delete!(chr) if chr.downcase == chr }
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  if str.length.odd? # odd length string
    str[mid]
  else # even length string
    str[(mid - 1)..mid]
  end
end

# Return the number of vowels in a string.
VOWELS = %w[a e i o u]
def num_vowels(str)
  str.downcase.count('aeiou')
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end

# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = '')
  result = ''
  arr.each_with_index do |el, idx|
    result += if idx == arr.length - 1
                el.to_s
              else
                (el.to_s + separator)
              end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  arr = str.chars
  arr.each_with_index do |ch, idx|
    arr[idx] = idx.odd? ? ch.upcase : ch.downcase
  end
  arr.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = str.split.map do |word|
    word.length >= 5 ? word.reverse : word
  end
  result.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |x|
    if x % 3 == 0 && x % 5 == 0
      'fizzbuzz'
    elsif x % 5 == 0
      'buzz'
    elsif x % 3 == 0
      'fizz'
    else
      x
    end
  end
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  rev = []
  rev.push(arr.pop) until arr.empty?
  rev
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  x = num - 1
  return false if num == 1
  while x > 1
    return false if (num % x).zero?
    x -= 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = (1..num).to_a
  arr.select { |e| (num % e).zero? }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |e| prime?(e) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = arr.select(&:odd?)
  even = arr.select(&:even?)
  odd.count < even.count ? odd[0] : even[0]
end
